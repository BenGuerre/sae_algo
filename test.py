from requetes import *

GRAPH = json_vers_nx_bride("data", 2)
def test_collaborateurs_communs():
    assert collaborateurs_communs(GRAPH.edges, "Núria Espert","Rosa Maria Sardà") == {"Anna Lizaran","Mercè Pons"}  
    assert collaborateurs_communs(GRAPH.edges, "ben", "truc") == set()
    assert collaborateurs_communs(GRAPH.edges, "Núria Espert", "Bruce Campbell") == set()

def test_collaborateurs_proches():
    assert collaborateurs_proches(GRAPH, "Núria Espert", 1) == {'Mercè Pons', 'Anna Lizaran', 'Núria Espert', 'Rosa Maria Sardà'}
    assert collaborateurs_proches(GRAPH, "ben", 3) == None
    GRAPH.add_edge("Núria Espert" , "Bruce Campbell")
    assert collaborateurs_proches(GRAPH, "Núria Espert", 2) == {'Patricia Tallman', 'Bill Moseley', 'Marcus Gilbert (actor)|Marcus Gilbert', 'Rosa Maria Sardà', 'Embeth Davidtz', 'Anna Lizaran', 'Ian Abercrombie', 'Bridget Fonda', 'Bruce Campbell', 'Michael Earl Reid', 'Ted Raimi', 'Mercè Pons', 'Angela Featherstone', 'Richard Grove', 'Timothy Patrick Quill', 'Núria Espert'}
    GRAPH.remove_edge("Núria Espert", "Bruce Campbell")

def test_est_proches():
    assert est_proches(GRAPH, "Núria Espert", 'Mercè Pons') 
    assert est_proches(GRAPH, "machin", "truc") is None
    assert not est_proches(GRAPH, "Núria Espert", "Bruce Campbell") 
    GRAPH.add_edge("Núria Espert" , "Bruce Campbell")
    assert est_proches(GRAPH, "Núria Espert", 'Bill Moseley', 2)
    GRAPH.remove_edge("Núria Espert", "Bruce Campbell")

def test_distance():
    assert distance(GRAPH, "Núria Espert", 'Mercè Pons') == 1
    assert distance(GRAPH, "karim", "olivier") == None
    GRAPH.add_edge("Núria Espert" , "Bruce Campbell")
    assert distance(GRAPH, "Núria Espert", 'Bill Moseley') == 2
    GRAPH.remove_edge("Núria Espert", "Bruce Campbell")

def test_centralite():
    assert centralite(GRAPH, "Núria Espert") == 3
    assert centralite(GRAPH, "jul") == None
    assert centralite(GRAPH, 'Bruce Campbell') == 3

def test_centre_hollywood():
    assert centre_hollywood(GRAPH) 