from cgitb import reset
import json
import re
import networkx as nx
import matplotlib.pyplot as plt


# Question 1
def json_vers_nx(chemin):
    """Regroupe permet de générer un graphique
        Compléxité: o(n**3)
    Args:
        chemin (str): chemin du fichier .txt (ne pas écrire .txt)
    Return:
        Graph: le graphe provenant des donnée du fichier 
    """
    charger_fichier(chemin)
    graph = nx.Graph()
    ensembleActeurs, ensembleLiaisons = liaison_acteurs(chemin)
    Creation_liaison(graph, ensembleActeurs, ensembleLiaisons)
    return graph

def json_vers_nx_bride(chemin, nbFilms=10):
    """Regroupe permet de générer un graphique avec un nombreux de film limité
        Compléxité: o(n**3)
    Args:
        chemin (str): chemin du fichier .txt (ne pas écrire .txt)
        nbFilms (int, optional): nombre de films à traiter. Default nombre de films contenue dans le fichier
    Return:
        Graph: le graphe provenant des donnée du fichier 
    """
    charger_fichier(chemin)
    graph = nx.Graph()
    ensembleActeurs, ensembleLiaisons = liaison_acteurs_bride(chemin, nbFilms)
    Creation_liaison(graph, ensembleActeurs, ensembleLiaisons)
    return graph

def charger_fichier(file_name):
    """Cette fonction convertie le fichier txt au format json
        Compléxité: o(n)
    Args:
        file_name (str): Nom du fichier (ne pas mettre le point .txt)
    """    
    with open(file_name + ".txt", 'r') as fichier:  # On ouvre le fichier principale
        with open(file_name + ".json", 'w') as file:  # On crée un fichier Json
            lignes = fichier.readlines()
            for ligne in lignes:
                #On supprime tout les crochets innutile du fichier
                ligne = re.sub("\[{2}|]{2}","", ligne)
                file.write(ligne)

# Les fonctions suivantes représentes la foctions json_vers_nx nous l'avons séparer en deux fonctions
# pour des questions de lisibilité et de bonne pratique (une fonction = une fonctionnalité)


def Creation_liaison(graph, nodes, edges):
    """Permet d'afficher un graphe
        Compléxité: o(1)


    Args:
        nodes (set): Ensemble des sommets
        edges (set): ensemble des arrêtes
    """
    #Ajoutes les sommets et les liasions à un graphe
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)


def liaison_acteurs (fileName):
    """Permet obtenir l'ensemble des acteurs et de leur colaborations avec d'autres acteurs
        Compléxité: o(n**3)
    Args:
        fileName (str): nom du fichier .json avec les données (ne pas mettre le .json)
        nbFilms (int, optional): nombre de films à traiter. Default nombre de films contenue dans le fichier
    Return:
        set: Ensemble des acteurs 
        set: Ensemble des colaborations
    """
    ensemble_liaison = set() #Initialisation de l'ensemble des liaison entre acteurs
    ensemble_acteurs = set() #Initialisation de l'ensemble des acteurs
    file = open(fileName + ".json","r") 
    for film in file: #Parcourt les films(= une ligne) du fichier
        film = json.loads(film)# Les met sous forme dict
        for acteur in film["cast"]:
            ensemble_acteurs.add(acteur)
            for acteur2 in film["cast"]:
                if acteur != acteur2:
                    # Création des liaisons entre les acteurs d'un même films
                    ensemble_liaison.add(frozenset((acteur,acteur2))) #Ajoute une ensemble frozenset = liaison et le met dans ensemble_liaison
    return ensemble_acteurs, ensemble_liaison


# Fonctions bridé permet de faire le graphe pour un nombre de films données 

def liaison_acteurs_bride (fileName,nbfilms=10):
    """Permet obtenir l'ensemble des acteurs et de leur colaborations
     avec d'autres acteurs pour un nombre de film données
    Compléxité: o(n**3)
    Args:
        fileName (str): nom du fichier .json avec les données
        nbFilms (int, optional): nombre de films à traiter. Default nombre de films contenue dans le fichier
    Return:
        set: Ensemble des acteurs 
        set: Ensemble des colaborations
    """
    ensemble_liaison = set() #Initialisation de l'ensemble des liaison entre acteurs
    ensemble_acteurs = set() #Initialisation de l'ensemble des acteurs
    file = open(fileName + ".json","r")  
    i = 0
    for film in file: #Parcourt les films(= une ligne) du fichier
        if i != nbfilms:
            film = json.loads(film)# Les met sous forme dict
            for acteur in film["cast"]:
                ensemble_acteurs.add(acteur)
                for acteur2 in film["cast"]:
                    if acteur != acteur2:
                        ensemble_liaison.add(frozenset((acteur,acteur2))) #Ajoute une ensemble frozenset = liaison et le met dans ensemble_liaison
            i += 1
        else:
            return ensemble_acteurs, ensemble_liaison


# Question 2

def collaborateurs_communs(set_collaborations, acteur1, acteur2):
    """Cette fonction renvoie l'ensemble des collaborations
     communs de deux acteurs données
    compléxité :O(n**3)

    Args:
        set_collaborations  (set): enseble des collborations
        acteur1 (String): Nom du première acteur
        acteur2 (String): Nom du deuxième acteur

    Returns:
        Set : Ensemble des acteurs avec qui ils ont collaboré
    """    
    ensemble_collaborateur_commun = set()
    dico_collab = {acteur1: set(), acteur2: set()}
    # On toutes les arrêtes du graphe
    for collaboration in set_collaborations:
        # On regarde si l'acteurs 1 ou l'acteur 2 est   dans cette arretes si oui on ajoutes 
        # l'acteur avec qui il collabore au dico de collaboration
        if acteur1 in collaboration:
            for personne in collaboration:
                if personne != acteur1:
                    dico_collab[acteur1].add(personne)
        elif acteur2 in collaboration:
            for personne in collaboration:
                if personne != acteur2:
                    dico_collab[acteur2].add(personne)
    #On regarde si les collaboration de l'acteurs 1 sont danss celle de l'acteurs deux et si
    #c'est le cas on les ajoutes au collaborations commune
    for acteur in dico_collab[acteur1]:
        if acteur in dico_collab[acteur2]:
            ensemble_collaborateur_commun.add(acteur)
    return ensemble_collaborateur_commun        


# Question 3

def collaborateurs_proches(G,u,k):
    """Fonction renvoyant l'ensemble des acteurs à distance au plus k de l'acteur u dans le graphe G. La fonction renvoie None si u est absent du graphe.
    complexité:O(n**4)
    Parametres:
        G: le graphe
        u: le sommet de départ
        k: la distance depuis u
    """
    #On vérifie que l'acteur est bien dans le graphe
    if u not in G.nodes:
        print(u,"est un illustre inconnu")
        return None
    collaborateurs = set()
    collaborateurs.add(u)
    # On parcours tout les voisins jusqu'aux voisins d'ordre k
    for i in range(k):
        collaborateurs_directs = set()
        #On parcours les collaborateur
        for c in collaborateurs:
            #On regarde ses voisins et les ajoutes aux collaborateurs si ils n'y sont pas déja
            for voisin in G.adj[c]:
                if voisin not in collaborateurs:
                    collaborateurs_directs.add(voisin)
        collaborateurs = collaborateurs.union(collaborateurs_directs)
    return collaborateurs

def est_proches(G,u,v,k=1):
    """Fonction renvoyant si deux acteurs sont voisins.
    complexité:O(n**4)
    Parametres:
        G: le graphe
        u: le sommet de départ
        v: le sommet d'arrivé
        k: la distance depuis u
    """
    # même principe mais pour un degrès 1
    if u not in G.nodes:
        print(u,"est un illustre inconnu")
        return None
    collaborateurs = set()
    collaborateurs.add(u)
    for i in range(k):
        collaborateurs_directs = set()
        for c in collaborateurs:
            for voisin in G.adj[c]:
                if voisin not in collaborateurs:
                    collaborateurs_directs.add(voisin)
        collaborateurs = collaborateurs.union(collaborateurs_directs)
    return v in collaborateurs


def distance(G,u,acteur2):
    """Fonction renvoyant la distance entre deux acteurs du graphe
    compléxité: O(n**3)
    Parametres:
        G: le graphe
        u: le sommet de départ
        acteur2: Nom du deuxième acteurs
    """
    distance = 0
    #On vérifie que les deux acteurs sont bien des sommets du graphe
    if u not in G.nodes or acteur2 not in G.nodes:
        return None
    collaborateurs = set()
    collaborateurs.add(u)
    #On effectue un parcours en largeur tant que l'acteur deux n'est pas trouvé
    while acteur2 not in collaborateurs:
        collaborateurs_directs = set()
        for c in collaborateurs:
            for voisin in G.adj[c]:
                collaborateurs_directs.add(voisin)
        distance += 1        
        collaborateurs = collaborateurs.union(collaborateurs_directs)
    return distance

#Question 4

def centralite(G,acteur1):
    """Cette fonction renvoie la taille de la plus grande 
    chaine qui sépare deux acteurs
    compléxité : O(n**3)
    Args:
        G (GRAPH): 
        acteur1 (String): Nom de l'acteur de départ

    Returns:
        int | None: taille de la chaine la plus grande entre deux acteurs
    """ 
    if acteur1 not in G.nodes:
        return None
    res = 0
    collaborateurs = set()
    collaborateurs.add(acteur1)
    collaborateurs_traité = set()
    en_cours = True
    #On effectue un parcous en largeur du graph qui à pour point de départ l'acteur1
    while en_cours:
        en_cours = False
        collaborateurs_directs = set()
        for c in collaborateurs:
            #Tant qu'il y à des voisins on les ajoute au collaborateur direct 
            for voisin in G.adj[c]:
                if voisin not in collaborateurs_traité:
                    collaborateurs_directs.add(voisin)
                    en_cours = True
                    collaborateurs_traité.add(voisin)
        # On incrémente de 1 pour savoir à quel degès de voisinnage nous sommes
        res += 1
        collaborateurs = collaborateurs.union(collaborateurs_directs)
    return res

def centre_hollywood(Graphe):
    """Cette fonction renvoie l'acteur centrale
    complexité:O(n**4)
    Args:
        G (GRAPH): 
    Returns:
        String: Nom de l'acteur central

    """
    #On fait la centralite sur chacun des acteurs et on garde l'acteur avec la plus petite
    min_centralite = None
    acteur_central = None
    for acteur in Graphe.nodes:
        centre = centralite(Graphe, acteur)
        if min_centralite == None or min_centralite > centre:
            min_centralite = centre
            acteur_central = acteur
    return acteur_central



    


#-----------------------------------------------Main

# # Création du Graphe

# GRAPH = json_vers_nx_bride("data", 2)
# print(GRAPH)


# # #Convertion du fichier txt en json

# # charger_fichier("data.txt")

# # # #Création des ensemble de sommet et d'arrete

# # ensembleActeurs, ensembleLiaisons = liaison_acteurs_bride("data.json",100)
# # Creation_liaison(GRAPH, ensembleActeurs, ensembleLiaisons)


# # #Collaborateur le plus proche et distance entre deux


# # print(collaborateurs_proches(GRAPH,"Núria Espert",1))
# # print(distance(GRAPH,"Núria Espert","Bob Harras"))

# # # Centralite 

# # print(centralite(GRAPH, "Rosa Maria Sardà"))


# # Affichage du graphe
# nx.draw(GRAPH, with_labels = True)
# plt.show()
