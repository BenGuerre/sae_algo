from matplotlib import pyplot as plt
import requetes
import networkx as nx

def start():
    """Lance l'interface d'utilisation
    """
    print("--------------------------------------------------")
    nom_fic = input("entre le nom du fichier texte(sans le .txt) \n")
    print("--------------------------------------------------")
    print("         Chargement de la base de donnée \n \n \n \n \n \n \n")
    GRAPH = requetes.json_vers_nx(nom_fic)
    print("--------------------------------------------------")
    print("               Chargement teminé\n \n ")
    fin = False
    while not fin:
        menu()
        choix = input("entrez votre choix \n")
        if choix == "1":
            acteur1 = input("Nom du premier acteur: \n")
            acteur2 = input("Nom du deuxieme acteur: \n")
            print("\n")
            colab_commun = requetes.collaborateurs_communs(GRAPH.edges, acteur1, acteur2)
            if len(colab_commun) > 0:
                for colaborateur in colab_commun:
                    print("   -" + colaborateur)
            else:
                print("Il n'ont pas de collaborateurs en communs \n")
            fin = menu_fin()

        elif choix == "2":
            acteur1 = input("Nom de l'acteur: \n")
            distance = input("A quel distance: \n")
            collab_proche = requetes.collaborateurs_proches(GRAPH, acteur1, int(distance))
            if collab_proche is not None:
                for collaborateur in collab_proche:
                    print("   -" + collaborateur)
            fin = menu_fin()

        elif choix == "3":
            acteur1 = input("Nom du premier acteur: \n")
            acteur2 = input("Nom du deuxieme acteur: \n")
            distance_collab = requetes.distance(GRAPH, acteur1, acteur2)
            if distance_collab is not None:
                print( str(distance_collab) + "\n")
            else:
                print("   Aucun collaborateur en commun \n")
            fin = menu_fin()

        elif choix == "4":
            nb_films = input("Combien de films voulez vous afficher? \n")
            print("              Chargement    \n")
            GRAPH2 = requetes.json_vers_nx_bride(nom_fic, nb_films)
            nx.draw(GRAPH2, with_labels = True)
            plt.show()
            fin = menu_fin()

        elif choix == "5":
            acteur1 = input("Entrez le nom de l'acteur: \n")
            print("\n" + acteur1 + "est au miximum à" + str(requetes.centralite(GRAPH, acteur1)) + " personnes d'un autre acteur \n")
            fin = menu_fin()
        
        elif choix == "6":
            print(requetes.centre_hollywood(GRAPH))
            fin = menu_fin()

        elif choix in ["q", "Q"]:
            print("Esitez pas à mettre un 20")
            fin = True
    
def menu():
    """Permet afficher le menu
    """
    print("--------------------------------------------------")
    print("                  Quoi faire?")
    print("1 -Trouvez les collaboraturs commun à deux acteurs \n")
    print("2 -Trouver les collaborateurs proches \n")
    print("3 -Trouvez la distance entres deux acteurs \n")
    print("4 -Afficher un graphe avec un certains nombre de films \n")
    print("5 -Afficher la centralité d'un acteur \n")
    print("6 -Afficher l'acteur centrale d'Hollywood \n")
    print("Q -Quitter \n")
    print(" \n")

def menu_fin():
    """Permet afficher la proposition de fin 
    Retuns:
        bool: true si veux quitter sinon faux
    """
    choix = None
    while choix not in ["o", "n", "N", "O"]:
        choix = input("continuer? (o/n) \n")
        if choix in ["n", "N"]:
            return True
    return False
    
start()
